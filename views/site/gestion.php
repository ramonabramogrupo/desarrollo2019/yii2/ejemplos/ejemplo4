<?php
use yii\helpers\Html;

echo Html::a('Ciclistas', 
        ['ciclista/index'], 
        ['class' => 'col-md-3 col-md-offset-2 btn btn-primary btn-large']
        );

echo Html::a('Etapas', 
        ['etapa/index'], 
        ['class' => 'col-md-3 col-md-offset-2 btn btn-primary btn-large']
        );

echo Html::a('Puertos', 
        ['puerto/index'], 
        ['class' => 'col-md-3 col-md-offset-2 btn btn-primary btn-large']
        );

echo Html::a('Equipos', 
        ['equipo/index'], 
        ['class' => 'col-md-3 col-md-offset-2 btn btn-primary btn-large']
        );

echo Html::a('Maillots', 
        ['maillot/index'], 
        ['class' => 'col-md-3 col-md-offset-2 btn btn-primary btn-large']
        );

echo Html::a('Lleva', 
        ['lleva/index'], 
        ['class' => 'col-md-3 col-md-offset-2 btn btn-primary btn-large']
        );


